# gitlab-tools

Helper tools for Gitlab

## glab-repo

For a new repository only. Should be used from a root directory of a project.

- it will name the project by directory name
- set remote *origin* to *gitlab*
- set *master* branch's upstream to *origin*
- push to gitlab

### Notes

Gitlab's repository will be private, so you might want to chage that.

## github2gitlab

A helper script to migrate a repository from Github to Gitlab.

### Usage

github2gitlab [github_url.git]

### Notes

Username or organisation/group in github and gitlab must match.

Gitlab's repository will be private, so you might want to chage that.

If this repository is a fork of another github repo, you might want to setup
a pull mirroring of the upstream repo in gitlab (use HTTPS address without password)
and add *upstream* remote locally.

Example of adding upstream locally:

```sh
git remote add upstream "git@github.com:repo/project.git"
```

You might want to change the default branch in gitlab

### Changing git history

Use `GITHUB_EMAIL` and `GITLAB_EMAIL` environment variables to replace author's and
commiter's email.

`./.env` file will be sourced, so you might want to set those variables there.
You can copy `./.env.sample` to `./.env` and edit it.
