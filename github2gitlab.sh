#!/bin/sh

DIR="$(realpath "$(CDPATH= cd -- "$(dirname -- $(readlink -f "$0"))" && pwd -P)")"
PROJECT="$(basename -s".git" "$1")"
URL_SFX="$(echo "$1" | sed -e 's#.\+github\.com.##')"

[ -f "$DIR/.env" ] && . "$DIR/.env"

if [ -z "$GITHUB_EMAIL" ]; then
    echo "Error: GITHUB_EMAIL not set"
    exit 1
elif [ -z "$GITLAB_EMAIL" ]; then
    echo "Error: GITLAB_EMAIL not set"
    exit 1
fi

[ -d "$PROJECT" ] || git clone --mirror "$1" "$PROJECT/.git"
cd "$PROJECT"

git config --bool core.bare false
git checkout HEAD
git config user.email "$GITLAB_EMAIL"

export GITHUB_EMAIL GITLAB_EMAIL

git filter-branch --env-filter '
if [ "$GIT_COMMITTER_EMAIL" = "$GITHUB_EMAIL" ]; then
    export GIT_COMMITTER_EMAIL="$GITLAB_EMAIL"
fi
if [ "$GIT_AUTHOR_EMAIL" = "$GITHUB_EMAIL" ]; then
    export GIT_AUTHOR_EMAIL="$GITLAB_EMAIL"
fi
' --tag-name-filter cat -- --branches --tags

git gc --prune=now --aggressive
git remote set-url origin "git@gitlab.com:$URL_SFX"
git push --mirror -u origin
