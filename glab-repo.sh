#!/bin/sh

DIR="$(realpath "$(CDPATH= cd -- "$(dirname -- $(readlink -f "$0"))" && pwd -P)")"

[ -f "$DIR/.env" ] && . "$DIR/.env"

PROJECT="$(basename "$(pwd)")"
URL_ORIGIN="git@gitlab.com:$GITLAB_USER/$PROJECT.git"

if [ -z "$GITLAB_USER" ]; then
    echo "Error: GITLAB_USER not set"
    exit 1
elif [ -z "$GITLAB_EMAIL" ]; then
    echo "Error: GITLAB_EMAIL not set"
    exit 1
fi

[ -d ".git" ] || git init

if [ "$( git branch )" = "" ]; then
    read -p "Make an initial commit with all files added to it [y/n]? " yn
fi

if [ "$yn" = "y" ]; then
    git add .
    git commit -m "Initial commit"
else
    echo "Error: make a first commit and run this command again"
    exit 1
fi

git config user.name "$GITLAB_USER"
git config user.email "$GITLAB_EMAIL"
git remote add origin "$URL_ORIGIN" 2> /dev/null

git push -u origin master
